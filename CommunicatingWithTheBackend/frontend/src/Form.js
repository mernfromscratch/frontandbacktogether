
const Form = ({ itemType, editOrCreate, sendItemToBack, setItem, item }) => {
    const formStyle = {display: 'inline-block', leftMargin: 'auto', rightMargin: 'auto'};

    const propertiesAndTypesWatches = [
        {propertyName: 'Brand', inputType: 'text'}, 
        {propertyName: 'Price', inputType: 'number'}, 
        {propertyName: 'Shininess', inputType: 'range', min: 0, max: 50}, 
        {propertyName: 'SmartWatch', inputType: 'checkbox'}];
    const propertiesAndTypesGames = [
            {propertyName: 'Brand', inputType: 'text'}, 
            {propertyName: 'Price', inputType: 'number'}, 
            {propertyName: 'Replayability', inputType: 'range'}, 
            {propertyName: 'Genre', inputType: 'text'},
            {propertyName: 'BloodAndGore', inputType: 'checkbox'}];
    const propertiesAndTypesToMap = itemType === 'watch' ? propertiesAndTypesWatches : propertiesAndTypesGames;

    const giveInputField = (inputDescription) => {
        const inputType = inputDescription.inputType;
        const propertyName = inputDescription.propertyName;
        const setItemCorrectly = (event) => {
            (inputType === 'checkbox') 
            ? setItem({...item, [propertyName]: event.target.checked})
            : setItem({...item, [propertyName]: event.target.value})
        };
        return (
            <tr key={propertyName}>
                <td style={{textAlign: 'left'}}>
                    {propertyName}
                </td>
                <td>
                    <input
                        type={inputType} 
                        value={item[propertyName]} 
                        onChange={setItemCorrectly} 
                        min={inputDescription.min} 
                        max={inputDescription.max} 
                        checked={(inputType === 'checkbox') ? (item[propertyName]) : false}
                    />
                </td>
            </tr>
        )
    };

    const send = (event) => {
        event.preventDefault();
        sendItemToBack()
    }

    return (
            <form style={formStyle}>
                <h1>{editOrCreate === 'create' ? 'Create' : 'Edit'}</h1>
                <table>
                    <tbody>
                        {propertiesAndTypesToMap.map( (inputDesc) => giveInputField(inputDesc) )}
                    </tbody>
                </table>
                <button type='submit' onClick={send}>Submit</button>
            </form>            
    );
};

export default Form;