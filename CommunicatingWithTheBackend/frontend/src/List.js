import React, {useState, useEffect} from 'react';
import {Link} from 'react-router-dom';

const List = ({displayItems, itemType, setItems}) => {
    const propertiesWatches = ['ID', 'Brand', 'Price', 'Shininess', 'SmartWatch'];
    const propertiesGames = ['ID', 'Brand', 'Price', 'Replayability', 'Genre', 'BloodAndGore'];
    const propertiesToMap = itemType === 'watch' ? propertiesWatches : propertiesGames;

    const surroundStyle = {display: 'inline-block', leftMargin: 'auto', rightMargin: 'auto', marginBottom: '30px', marginTop: '30px'};
    const tableStyle = {border: '1px solid #BBBBBB', borderCollapse: 'collapse'};
    const rowStyle = {borderLeft: '1px solid #BBBBBB', borderRight: '1px solid #BBBBBB', paddingLeft: '10px', paddingRight: '10px'};

    // const watchTest = [{ID: 1, Brand: 'Werbly', Price: 500, Shininess: 35, SmartWatch: true}];

    const deleteItem = (ID) => {
        const deleteUrl = 'http://localhost:8000/api/' + itemType;
        fetch(deleteUrl, {
            method: 'DELETE', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify({ ID }) // body data type must match "Content-Type" header
        })
        .then((data) => data.json())
        .then((deletedItem) => {
            const itemsWithoutDeletedOne = displayItems.filter((item, index, arr) => {
                return item.ID !== deletedItem.ID;
            });
            setItems(itemsWithoutDeletedOne);
        });
    };

    return (
        <div style={surroundStyle}>
            <table style={tableStyle}>
                <thead>
                    <tr style={tableStyle}>
                        {propertiesToMap.map((property) => (
                                <th key={property} style={rowStyle}>
                                    {property}
                                </th>
                            ))
                        }
                        <th style={rowStyle}>
                            Actions
                        </th>
                    </tr>
                </thead>
                {
                    (displayItems.length > 0)
                    ? (
                        <tbody>
                            {displayItems.map((item) => (
                                <tr style={tableStyle} key={item.ID}>
                                    {propertiesToMap.map((property) => (
                                            <td style={rowStyle} key={property}>
                                                {(item[property] === true|item[property] === false) ? ((item[property] === true) ? "Yes" : "No") : item[property]}
                                            </td>
                                    ))}
                                    <td style={rowStyle}>
                                        <Link to={'/edit/' + itemType + '/' + item.ID}> 
                                            <button>
                                                Edit
                                            </button>
                                        </Link>
                                        <button onClick={() => deleteItem(item.ID)}>
                                            Delete
                                        </button> 
                                    </td>
                                </tr>
                            ))}
                        </tbody>
                    )
                    : <tbody></tbody>
                }
            </table>
        </div>
    );
};

export default List;