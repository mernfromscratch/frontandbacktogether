import React, {useState, useEffect} from 'react';
import { Link } from 'react-router-dom';
import List from './List.js';

const Home = () => {
    const [watches, setWatches] = useState([]);
    const [games, setGames] = useState([]);

    const linkStyle = { borderRadius: '20px', backgroundColor: '#8888FF', margin: '5px', padding: '5px', textDecoration: 'none', color: 'white' };

    useEffect(() => {
        const urlToWatches = 'http://localhost:8000/api/watches/all';
        const urlToGames = 'http://localhost:8000/api/games/all';
        
        fetch(urlToWatches)
            .then((data) => data.json())
            .then((json) => setWatches(json));

        fetch(urlToGames)
            .then(data => data.json())
            .then(json => setGames(json));
    }, []);

    useEffect(() => {}, [watches, games]);

    return (
        <div style={{ paddingTop: '50px' }}>
            <Link to="/create/watch" style={linkStyle}>
                Add watch
            </Link>
            <Link to="/create/game" style={linkStyle}>
                Add game
            </Link>
            <br />
            <List displayItems={watches} itemType={'watch'} setItems={setWatches} />
            <br />
            <List displayItems={games} itemType={'game'} setItems={setGames} />
        </div>
    );
};

export default Home;