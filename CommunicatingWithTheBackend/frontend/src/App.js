import {Switch, Route} from 'react-router-dom';
import Home from './Home';
import Edit from './Edit';
import Create from './Create';
import './App.css';

function App() {
  return (
    <div className="App">
      <Switch>
        <Route exact path="/" component={Home} />
        <Route path="/edit/:itemType/:ID" component={Edit} />
        <Route path="/create/:itemType" component={Create} />
      </Switch>
    </div>
  );
}

export default App;
