import React, {useState, useEffect} from 'react';
import { useParams } from 'react-router-dom';
import Form from './Form';
 
 const Edit = ({ history }) => {
    const params = useParams();
    const itemType = params.itemType;
    const ID = params.ID;
    const [watch, setWatch] = useState({Brand: '', Price: 0, Shininess: 25, SmartWatch: false});
    const [game, setGame] = useState({Brand: '', Price: 0, Replayability: 5, Genre: '', BloodAndGore: false});

    useEffect(() => {
        const urlToGetItemFrom = 'http://localhost:8000/api/' + itemType + '/' + ID;
        fetch(urlToGetItemFrom)
        .then(res => res.json())
        .then(json => {
            if (json.ID !== undefined) {
                itemType === 'watch' ? setWatch(json) : setGame(json);
            }
            else {
                const urlOfCreate = '/create/' + itemType + '/';
                history.push(urlOfCreate);
            }
        })
    }, []);

    const sendToBackForEdit = () => {
        const urlToEditItem = 'http://localhost:8000/api/' + itemType + '/';
        fetch(urlToEditItem, {
            method: 'PUT',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(itemType === 'watch' ? watch : game) // body data type must match "Content-Type" header
        })
        .then(res => history.push('/'))
    };


    return (
        <Form itemType={itemType} editOrCreate={'edit'} sendItemToBack={sendToBackForEdit} setItem={itemType === 'watch' ? setWatch : setGame} item={itemType === 'watch' ? watch : game}  />
    )
};

export default Edit;