import React, {useState, useEffect} from 'react';
import { useParams } from 'react-router-dom';
import Form from './Form';

const Create = ({ history }) => {
    const params = useParams();
    const itemType = params.itemType;
    const [watch, setWatch] = useState({Brand: '', Price: 0, Shininess: 25, SmartWatch: false});
    const [game, setGame] = useState({Brand: '', Price: 0, Replayability: 5, Genre: '', BloodAndGore: false});

    const sendItemToBack = () => {
        const urlToSendTo = 'http://localhost:8000/api/' + itemType + '/';
        const itemToSend = itemType === 'watch' ? watch : game;
        fetch(urlToSendTo, {
            method: 'POST', // *GET, POST, PUT, DELETE, etc.
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json'
              },
            body: JSON.stringify(itemToSend) // body data type must match "Content-Type" header
        })
        .then((data) => {
            history.push('/');
        })
        .catch((err) => {
            console.log("Error:", err);
        });
    };

    return (
        <Form itemType={itemType} editOrCreate={'create'} sendItemToBack={sendItemToBack} setItem={itemType === 'watch' ? setWatch : setGame} item={itemType === 'watch' ? watch : game}  />
    )
};

export default Create;