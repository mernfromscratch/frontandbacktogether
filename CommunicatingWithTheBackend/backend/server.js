const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();

app.use(cors());
app.use(bodyParser.json({ limit: "2mb" }));

let messages = [];
let ID_num = 0;

let watches = [];
let ID_watches = 0;
let games = [];
let ID_games = 0;


// app.get('/api', function (req, res) {
//     res.json(messages);
// })
// app.post('/api', function (req, res) {
//     messages.push({ID: ID_num, message: req.body.message});
//     ID_num += 1;
//     res.json(messages[messages.length - 1]);
// })
// app.put('/api', function (req, res) {
//     res.json({text:'Hello World!'});
// })
// app.delete('/api', function (req, res) {
//     res.json({text:'Hello World!'});
// })

app.get('/api/watches/all', function (req, res) {
    res.json(watches);
})
app.get('/api/watch/:ID', function (req, res) {
    const ID = req.params.ID;
    let watch = {};
    for (let watch_num = 0; watch_num < watches.length; watch_num++) {
        if (watches[watch_num].ID == ID) {
            watch = watches[watch_num];
            break;
        }
    };
    res.json(watch);
       
});
app.post('/api/watch/', function (req, res) {
    let watch = req.body;
    watches.push({...watch, ID: ID_watches});
    ID_watches += 1;
    res.json(watches[watches.length - 1]);
})
app.put('/api/watch/', function (req, res) {
    let watch = req.body;
    let watchFromArray = {};
    for (let watch_num = 0; watch_num < watches.length; watch_num++) {
        if (watches[watch_num].ID == watch.ID) {
            watches[watch_num] = watch;
            watchFromArray = watches[watch_num];
            break;
        }
    };
    res.json(watchFromArray);

});
app.delete('/api/watch/', function (req, res) {
    let ID = req.body.ID;
    let deleted_watch = {};
    watches = watches.filter(function(watch, index, arr){ 
        if (watch.ID == ID) {
            deleted_watch = watch;
        }
        return watch.ID !== ID;
    });
    res.json(deleted_watch);
})

app.get('/api/games/all', function (req, res) {
    res.json(games);
})
app.get('/api/game/:ID', function (req, res) {
    const ID = req.params.ID;
    let game = {};
    for (let game_num = 0; game_num < games.length; game_num++) {
        if (games[game_num].ID == ID) {
            game = games[game_num];
            break;
        }
    };
    res.json(game);
       
});
app.post('/api/game/', function (req, res) {
    let game = req.body;
    games.push({...game, ID: ID_games});
    ID_games += 1;
    res.json(games[games.length - 1]);
})
app.put('/api/game/', function (req, res) {
    let game = req.body;
    for (let game_num = 0; game_num < games.length; game_num++) {
        if (games[game_num].ID == game.ID) {
            games[game_num] = game;
            res.json(games[game_num]);
            break;
        }
    }   
});
app.delete('/api/game/', function (req, res) {
    let ID = req.body.ID;
    let deleted_game = {};
    games = games.filter(function(game, index, arr){ 
        if (game.ID == ID) {
            deleted_game = game;
        }
        return game.ID !== ID;
    });
    res.json(deleted_game);
})

//port
const port = 8000 || process.env.PORT;

//start server
app.listen(port, () => console.log(`Server is now running on port ${port}`));